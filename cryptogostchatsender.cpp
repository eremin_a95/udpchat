#include "cryptogostchatsender.h"
#include <string>

CryptoGostChatSender::CryptoGostChatSender(ChatSender * chatSender, const QString & psk) : ChatSenderDecorator(chatSender)
{
	_key = SecByteBlock(0x00, GOST::DEFAULT_KEYLENGTH);
	SHA256().CalculateTruncatedDigest(_key, GOST::DEFAULT_KEYLENGTH, (byte *)psk.toStdString().data(), psk.length()); //TODO: Streebog needed
	_iv = SecByteBlock(GOST::BLOCKSIZE);
	_rnd.GenerateBlock(_iv, GOST::BLOCKSIZE);
//	qDebug() << "Pre-shared key: " << psk << "(length: " << psk.length() << ")";
//	qDebug() << "Sender key: " << QByteArray((char *)_key.BytePtr(), GOST::DEFAULT_KEYLENGTH);
	_enc = std::unique_ptr<CFB_Mode<GOST>::Encryption>(new CFB_Mode<GOST>::Encryption(_key, _key.size(), _iv));
//	qDebug() << "CryptoGostChatSender created";
}

CryptoGostChatSender::~CryptoGostChatSender()
{
//	qDebug() << "CryptoGostChatSender destroyed";
}

inline void CryptoGostChatSender::sendMsg(const QString & msg)
{
	sendMsg(msg.toUtf8());
}

inline void CryptoGostChatSender::sendMsg(const QByteArray & msg)
{
	memcpy(_buffer, _iv, GOST::BLOCKSIZE);
//	memcpy(_buffer + GOST::BLOCKSIZE, msg.data(), msg.size()); //Without encryption
	_enc->ProcessData(_buffer + GOST::BLOCKSIZE, (byte *)msg.data(), msg.size());
//	qDebug() << "Key: " << QByteArray((char *)_key.BytePtr());
//	qDebug() << "IV: " << QByteArray((char *)_iv.BytePtr(), _iv.size());
//	qDebug() << "Data: " << QByteArray((char *)_buffer + GOST::BLOCKSIZE, msg.size());
//	qDebug() << "Origin massage: " << msg;
	_chatSender->sendMsg(QByteArray((char *)_buffer, msg.size() + GOST::BLOCKSIZE));
	_rnd.GenerateBlock(_iv, GOST::BLOCKSIZE);
	_enc->SetKeyWithIV(_key, _key.size(), _iv);
}
