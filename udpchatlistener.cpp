#include "udpchatlistener.h"
#include <QNetworkDatagram>

inline void UdpChatListener::recv()
{
	while (_socket->hasPendingDatagrams())
	{
		const QNetworkDatagram & datagram = _socket->receiveDatagram();
		emit messageRecieved(datagram.data(), datagram.senderAddress(), datagram.senderPort());
	}
}

UdpChatListener::UdpChatListener(quint16 localPort)
{
	_socket = new QUdpSocket(this);

	if ( !(_socket->bind(QHostAddress::Any, localPort)) )
	{
		qDebug() << "Cant bind socket on " << localPort << " local UDP port!";
		delete _socket;
		return;
	}

	connect(_socket, SIGNAL(readyRead()), this, SLOT(recv()));

//	qDebug() << "UdpChatListener destroyed";
//	qDebug() << "Listen on UDP port " << localPort;
}

UdpChatListener::UdpChatListener(QUdpSocket * socket) : _socket(socket)
{
	connect(_socket, SIGNAL(readyRead()), this, SLOT(recv()));
}

UdpChatListener::~UdpChatListener()
{
	if (_socket != nullptr)
	{
		_socket->abort();
		disconnect(_socket, SIGNAL(readyRead()), this, SLOT(recv()));
		delete _socket;
	}
//	qDebug() << "UdpChatListener destroyed";
//	qDebug() << "Stop listening";
}

inline QUdpSocket * UdpChatListener::socket()
{
	return _socket;
}
