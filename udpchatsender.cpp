#include "udpchatsender.h"
#include <QHostAddress>
#include <QNetworkDatagram>

UdpChatSender::UdpChatSender(const QHostAddress & remoteAddr, quint16 remotePort, QAbstractSocket * socket) : ChatSender(remoteAddr, remotePort)
{
	if ((socket == nullptr) || (!std::is_same<decltype((QUdpSocket *)socket), class QUdpSocket *>::value))
	{
		_socket = new QUdpSocket(this);
		_isSocketPrivate = true;
		_socket->bind();
	}
	else
	{
		_socket = (QUdpSocket *)socket;
		_isSocketPrivate = false;
	}
//	qDebug() << "UdpChatSender created";
//	qDebug() << "Ready to send messages to " << QHostAddress(remoteAddr.toIPv4Address()).toString() << ':' << remotePort;
}

UdpChatSender::~UdpChatSender()
{
	if (_isSocketPrivate)
		delete _socket;
//	qDebug() << "UdpChatSender destroyed";
}

inline void UdpChatSender::sendMsg(const QString & msg)
{
	_socket->writeDatagram(msg.toUtf8(), _remoteAddr, _remotePort);
}

inline void UdpChatSender::sendMsg(const QByteArray & msg)
{
	_socket->writeDatagram(msg, _remoteAddr, _remotePort);
}
