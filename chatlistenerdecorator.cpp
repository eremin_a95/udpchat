#include "chatlistenerdecorator.h"

inline void ChatListenerDecorator::recv()
{
	_chatListener->recv();
}

ChatListenerDecorator::ChatListenerDecorator(ChatListener * chatListener) : _chatListener(chatListener)
{

}

ChatListenerDecorator::~ChatListenerDecorator()
{

}

inline QAbstractSocket * ChatListenerDecorator::socket()
{
	return _chatListener->socket();
}
