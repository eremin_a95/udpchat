#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "cryptogostchatlistener.h"
#include "cryptogostchatsender.h"
#include "udpchatlistener.h"
#include "udpchatsender.h"

namespace Ui {


class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void onMessageRecieved(const QByteArray & msg, const QHostAddress & addr, quint16 port);

	void on_acceptButton_clicked();

	void on_cancelButton_clicked();

	void on_sendButton_clicked();

	void on_msgEdit_returnPressed();

	void on_cryptoCheckBox_stateChanged(int arg1);

private:
	Ui::MainWindow * ui;
	ChatListener * listener;
	ChatSender * sender;

	void sendMsg(const QString & msg);
	void changeState();
	void printText(const QString & text);
};

#endif // MAINWINDOW_H
