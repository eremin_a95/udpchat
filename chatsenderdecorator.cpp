#include "chatsenderdecorator.h"

ChatSenderDecorator::ChatSenderDecorator(ChatSender * chatSender) : ChatSender(QHostAddress::Any, 0), _chatSender(chatSender)
{

}

ChatSenderDecorator::~ChatSenderDecorator()
{

}

inline void ChatSenderDecorator::sendMsg(const QString & msg)
{
	_chatSender->sendMsg(msg);
}

inline void ChatSenderDecorator::sendMsg(const QByteArray & msg)
{
	_chatSender->sendMsg(msg);
}
