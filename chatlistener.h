#ifndef CHATLISTENER_H
#define CHATLISTENER_H

#include <QThread>
#include <QHostAddress>
#include <QAbstractSocket>

class ChatListener : public QThread
{
	Q_OBJECT

private:
public slots:
	virtual void recv() = 0;

public:
	ChatListener();
	virtual ~ChatListener();

	void run() override;

	virtual QAbstractSocket * socket() = 0;

signals:
	void messageRecieved(const QByteArray & msg, const QHostAddress & addr, quint16 port);
};

#endif // CHATLISTENER_H
