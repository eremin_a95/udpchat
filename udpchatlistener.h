#ifndef UDPCHATLISTENER_H
#define UDPCHATLISTENER_H
#include <chatlistener.h>
#include <QUdpSocket>

class UdpChatListener : public ChatListener
{
private:
	QUdpSocket * _socket; //TODO: change to shared_ptr

public slots:
	virtual void recv() override;

public:
	UdpChatListener(quint16 localPort);
	UdpChatListener(QUdpSocket * socket);
	virtual ~UdpChatListener();

	virtual QUdpSocket * socket() override;
};

#endif // UDPCHATLISTENER_H
