#ifndef CHATLISTENERDECORATOR_H
#define CHATLISTENERDECORATOR_H

#include "chatlistener.h"
#include <memory>

class ChatListenerDecorator : public ChatListener
{
protected:
	std::shared_ptr<ChatListener> _chatListener;

public slots:
	virtual void recv() override;

public:
	ChatListenerDecorator(ChatListener * chatListener);
	virtual ~ChatListenerDecorator();

	virtual QAbstractSocket * socket() override;
};

#endif // CHATLISTENERDECORATOR_H
