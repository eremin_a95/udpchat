#ifndef CHATSENDER_H
#define CHATSENDER_H

#include <QThread>
#include <QHostAddress>

class ChatSender : public QThread
{
protected:
	QHostAddress _remoteAddr;
	quint16 _remotePort;

public:
	ChatSender(const QHostAddress & remoteAddr, quint16 remotePort);
	virtual ~ChatSender();


	void run() override;

	virtual void sendMsg(const QString & msg) = 0;
	virtual void sendMsg(const QByteArray & msg) = 0;
};

#endif // CHATSENDER_H
