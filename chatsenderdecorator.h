#ifndef CHATSENDERDECORATOR_H
#define CHATSENDERDECORATOR_H

#include "chatsender.h"
#include <memory>

class ChatSenderDecorator : public ChatSender
{
protected:
	std::shared_ptr<ChatSender> _chatSender;
public:
	ChatSenderDecorator(ChatSender * chatSender);
	virtual ~ChatSenderDecorator();

	virtual void sendMsg(const QString & msg) override;
	virtual void sendMsg(const QByteArray & msg) override;
};

#endif // CHATSENDERDECORATOR_H
