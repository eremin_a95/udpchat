#ifndef CRYPTOGOSTCHATSENDER_H
#define CRYPTOGOSTCHATSENDER_H

#include "chatsenderdecorator.h"
#include <crypto++/gost.h>
#include <crypto++/modes.h>
#include <crypto++/sha.h>
#include <crypto++/osrng.h>

using namespace CryptoPP;

class CryptoGostChatSender : public ChatSenderDecorator
{
private:
	byte _buffer[512];
	SecByteBlock _key, _iv;
	AutoSeededRandomPool _rnd;
	std::unique_ptr<CFB_Mode<GOST>::Encryption> _enc;

public:
	CryptoGostChatSender(ChatSender * chatSender, const QString & psk);
	virtual ~CryptoGostChatSender();

	virtual void sendMsg(const QString & msg) override;
	virtual void sendMsg(const QByteArray & msg) override;
};

#endif // CRYPTOGOSTCHATSENDER_H
