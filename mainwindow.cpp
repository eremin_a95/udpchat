#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QNetworkDatagram>
#include <string>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	listener = nullptr;
	sender = nullptr;
	ui->remoteAddrEdit->setInputMask("000.000.000.000");
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_acceptButton_clicked()
{
	if (listener == nullptr)
	{
		listener = new UdpChatListener(ui->localPortEdit->text().toUInt());
		if (listener == nullptr)
			return;
	}

	if (sender == nullptr)
	{
		sender = new UdpChatSender(QHostAddress(ui->remoteAddrEdit->text()), ui->remotePortEdit->text().toUInt(), listener->socket());
		if (sender == nullptr)
		{
			delete listener;
			return;
		}
	}

	if (ui->cryptoCheckBox->isChecked())
	{
		sender = new CryptoGostChatSender(sender, ui->secretEdit->text());
		listener = new CryptoGostChatListener(listener, ui->secretEdit->text());
	}

	connect(listener, &ChatListener::messageRecieved, this, &MainWindow::onMessageRecieved);
	connect(listener, &ChatListener::finished, listener, &QObject::deleteLater);

	changeState();
}

void MainWindow::onMessageRecieved(const QByteArray & msg, const QHostAddress & addr, quint16 port)
{
	printText(QString("(") + QHostAddress(addr.toIPv4Address()).toString() + ":" + std::to_string(port).c_str() + "): " + msg);
}

void MainWindow::on_cancelButton_clicked()
{
	if (listener != nullptr)
	{
		listener->exit();
		disconnect(listener, &ChatListener::messageRecieved, this, &MainWindow::onMessageRecieved);
		disconnect(listener, &ChatListener::finished, listener, &QObject::deleteLater);
		delete listener;
		listener = nullptr;
	}

	if (sender != nullptr)
	{
		sender->exit();
		delete sender;
		sender = nullptr;
	}

	changeState();
	ui->secretEdit->setEnabled(ui->cryptoCheckBox->isChecked());
	ui->secretLabel->setEnabled(ui->cryptoCheckBox->isChecked());
}

void MainWindow::on_sendButton_clicked()
{
	if (ui->msgEdit->text().length() > 0)
		sendMsg(ui->msgEdit->text());
}

void MainWindow::on_msgEdit_returnPressed()
{
	if (ui->msgEdit->text().length() > 0)
		sendMsg(ui->msgEdit->text());
}

void MainWindow::sendMsg(const QString & msg)
{
	if (sender != nullptr)
	{
		sender->sendMsg(msg);
		ui->msgEdit->clear();
		printText(QString("You:") + msg);
	}
}

void MainWindow::changeState()
{
	bool state = ui->acceptButton->isEnabled();

	ui->acceptButton->setEnabled(!state);
	ui->localPortLabel->setEnabled(!state);
	ui->localPortEdit->setEnabled(!state);
	ui->remoteLabel->setEnabled(!state);
	ui->remoteAddrLabel->setEnabled(!state);
	ui->remotePortLabel->setEnabled(!state);
	ui->remotePortEdit->setEnabled(!state);
	ui->remoteAddrEdit->setEnabled(!state);
	ui->protoComboBox->setEnabled(!state);
	ui->secretEdit->setEnabled(!state);
	ui->secretLabel->setEnabled(!state);
	ui->cryptoCheckBox->setEnabled(!state);

	ui->cancelButton->setEnabled(state);
	ui->sendButton->setEnabled(state);
	ui->msgEdit->setEnabled(state);

}

void MainWindow::printText(const QString & text)
{
	ui->chatList->addItem(text);
	ui->chatList->scrollToBottom();
}

void MainWindow::on_cryptoCheckBox_stateChanged(int arg1)
{
	ui->secretEdit->setEnabled(arg1);
	ui->secretLabel->setEnabled(arg1);
}
