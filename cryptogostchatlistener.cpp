#include "cryptogostchatlistener.h"


inline void CryptoGostChatListener::recv()
{
	_chatListener->recv();
}

void CryptoGostChatListener::onMessageRecieved(const QByteArray & msg, const QHostAddress & addr, quint16 port)
{
	_iv = SecByteBlock((byte *)msg.data(), GOST::BLOCKSIZE);
	_dec->SetKeyWithIV(_key, GOST::DEFAULT_KEYLENGTH, _iv);
//	memcpy(_buffer, msg.data() + GOST::BLOCKSIZE, msg.size() - GOST::BLOCKSIZE); //Without decryption
	_dec->ProcessData(_buffer, (byte *)msg.data() + GOST::BLOCKSIZE, msg.size() - GOST::BLOCKSIZE);
	emit messageRecieved(QByteArray((char *)_buffer, msg.size() - GOST::BLOCKSIZE), addr, port);
//	qDebug() << "Key: " << QByteArray((char *)_key.BytePtr(), GOST::DEFAULT_KEYLENGTH);
//	qDebug() << "IV: " << QByteArray(msg.data(), GOST::BLOCKSIZE);
//	qDebug() << "Data: " << QByteArray(msg.data() + GOST::BLOCKSIZE, msg.size() - GOST::BLOCKSIZE);
//	qDebug() << "Decrypted message: " << QByteArray((char *)_buffer, msg.size() - GOST::BLOCKSIZE);
}

CryptoGostChatListener::CryptoGostChatListener(ChatListener * chatListener, const QString & psk) : ChatListenerDecorator(chatListener)
{
	_key = SecByteBlock(0x00, GOST::DEFAULT_KEYLENGTH);
	SHA256().CalculateTruncatedDigest(_key, GOST::DEFAULT_KEYLENGTH, (byte *)psk.toStdString().data(), psk.length());
	_dec = std::unique_ptr<CFB_Mode<GOST>::Decryption>(new CFB_Mode<GOST>::Decryption());
//	qDebug() << "Pre-shared key :" << psk << "(length: " << psk.length() << ")";
//	qDebug() << "Listener key: " << QByteArray((char *)_key.BytePtr(), GOST::DEFAULT_KEYLENGTH);
	connect(_chatListener.get(), &ChatListener::messageRecieved, this, &CryptoGostChatListener::onMessageRecieved);
//	qDebug() << "CryptoGostChatListener created";
}

CryptoGostChatListener::~CryptoGostChatListener()
{
	disconnect(_chatListener.get(), &ChatListener::messageRecieved, this, &CryptoGostChatListener::onMessageRecieved);
//	qDebug() << "CryptoGostChatListener destroyed";
}
