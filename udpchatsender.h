#ifndef UDPCHATSENDER_H
#define UDPCHATSENDER_H
#include <chatsender.h>
#include <QUdpSocket>

class UdpChatSender : public ChatSender
{
private:
	QUdpSocket * _socket;
	bool _isSocketPrivate;
public:
	UdpChatSender(const QHostAddress & remoteAddr, quint16 remotePort, QAbstractSocket * socket = nullptr);
	virtual ~UdpChatSender();

	virtual void sendMsg(const QString & msg) override;
	virtual void sendMsg(const QByteArray & msg) override;
};

#endif // UDPCHATSENDER_H
