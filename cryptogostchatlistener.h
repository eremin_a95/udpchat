#ifndef CRYPTOGOSTCHATLISTENER_H
#define CRYPTOGOSTCHATLISTENER_H

#include "chatlistenerdecorator.h"

#include <crypto++/gost.h>
#include <crypto++/sha.h>
#include <crypto++/modes.h>

using namespace CryptoPP;

class CryptoGostChatListener : public ChatListenerDecorator
{
private:
	byte _buffer[512];
	SecByteBlock _key, _iv;
	std::unique_ptr<CFB_Mode<GOST>::Decryption> _dec;
public slots:
	virtual void recv() override;
	void onMessageRecieved(const QByteArray & msg, const QHostAddress & addr, quint16 port);
public:
	CryptoGostChatListener(ChatListener * chatListener, const QString & psk);
	virtual ~CryptoGostChatListener();

};

#endif // CRYPTOGOSTCHATLISTENER_H
